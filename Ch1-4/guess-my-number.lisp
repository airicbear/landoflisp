;; Define global mutable variables
(defparameter *small* 1)
(defparameter *big* 100)

(defun guess-my-number ()
    (ash (+ *small* *big*) -1)) ; Arithmetic shift to the right (divide by 2)

(defun smaller ()
    (setf *big* (1- (guess-my-number))) ; Decrement big number limit
    (guess-my-number))

(defun bigger ()
    (setf *small* (1+ (guess-my-number))) ; Increment small number limit
    (guess-my-number))

(defun start-over ()
    (defparameter *small* 1)
    (defparameter *big* 100)
    (guess-my-number))