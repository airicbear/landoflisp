;;; Recursively increment my-length until it reaches an empty cons cell
;;; then return the total number of increments, or the length of the list
(defun my-length (list)
    (if list
        (1+ (my-length (cdr list)))
        0))