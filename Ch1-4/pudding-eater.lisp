(defvar *arch-enemy* nil)

(defun pudding-eater (person)
    (cond (
        ;; If person equals henry
        (eq person 'henry)
            (setf *arch-enemy* 'stupid-lisp-alien)
            '(curse you lisp alien - you ate my pudding))

        ;; If person equals johnny
        ((eq person 'johnny)
            (setf *arch-enemy* 'useless-old-johnny)
            '(i hope you choked on my pudding johnny))

        ;; If anyone else
        (t '(why you eat my pudding stranger ?))))

;; Using (case) instead of (cond)
(defun pudding-eater (person)
    (case person
        ((henry)
            (setf *arch-enemy* 'stupid-lisp-alien)
            ('curse you lisp alien - you ate my pudding))
        ((johnny)
            (setf *arch-enemy* 'useless-old-johnny))
        (otherwise '(why you eat my pudding stranger ?))))